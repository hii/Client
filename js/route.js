var myApp = angular.module('myApp', ['ngRoute'])

myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/search', {
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
     .when('/detail/:id', {
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/favourites', {
		  templateUrl: 'templates/favourites.html',
      controller: 'favouritesController'
		})
		.otherwise({
		  redirectTo: 'search'
		});
	


}]);
myApp.controller('searchController', function($scope, $http) {
  $scope.message = 'This is the search screen';
  $scope.search = function($event) {
    console.log('search()')
    if ($event.which == 13) { // enter key presses
      var search = $scope.searchTerm
      console.log(search)
      var url = 'https://api-simasjurkus.c9users.io/movies?title='+search                       //my api url
      $http.get(url).success(function(response) {
        console.log(response)
        $scope.movies = response.data
        $scope.searchTerm = ''
      })
    }
  }
})

myApp.controller('detailController', function($scope, $routeParams) {
  $scope.message = 'This is the detail screen'
  $scope.data = $routeParams.data
  $scope.addToFavourites = function(data) {
    console.log('adding: '+data+' to favourites.')
    localStorage.setItem(data, data)
  }
})
myApp.controller('favouritesController', function($scope) {
  console.log('fav controller')
  $scope.message = 'This is the favourites screen'
  $scope.delete = function(movie) {
    console.log('deleting id '+movie)
  }
  var init = function() {
    console.log('getting movies')
    var data = Array()
    for (var a in localStorage) {
      data.push(localStorage[a])
    }
    console.log(data)
    $scope.movies = data
  }
  init()
})